*** Settings ***

Suite Setup     Test suite setup
Suite Teardown  Test suite teardown
Test Setup      setup
Test Teardown   teardown

Library    QAutoLibrary.QAutoRobot    ${TESTDATA}

*** Variables ***

${url}     https://demo.qautomate.fi
${section_name}  demologin

*** Test Cases ***
Test example1

    ${section}=  Get section  ${section_name}
    Open application url   ${url}
    Input details  ${section}

*** Keywords ***
setup
    log  setup
    Start recording  ${TEST NAME}

teardown
    ${failure_image_path}=  Get failure image path  ${TEST NAME}
    Run Keyword If Test Failed  Take full screenshot  ${failure_image_path}

    Stop recording
    
    ${documentation}=  Generate failure documentation  ${TEST_DOCUMENTATION}  ${TEST NAME}
    Run Keyword If Test Failed  Set test documentation  ${documentation}
    
Test suite setup
    ${DefaultBrowser}=  Open browser  ${BROWSER}
    Set suite variable  ${DefaultBrowser}  ${DefaultBrowser}

Test suite teardown
    Close all browsers


